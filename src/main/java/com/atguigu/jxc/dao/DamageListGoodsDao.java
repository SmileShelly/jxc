package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface DamageListGoodsDao {
    Integer saveDamageList(DamageList damageList);

    Integer saveDamageListGoods(@Param("damageListGoodsList") List<DamageListGoods> damageListGoodsList, @Param("damageListId") Integer damageListId);

    List<DamageList> list(@Param("sTime") String sTime, @Param("eTime") String eTime);

    List<DamageListGoods> goodsList(@Param("damageListId") Integer damageListId);
}
