package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.CustomerDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.service.CustomerService;
import com.atguigu.jxc.service.LogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class CustomerServiceImpl implements CustomerService {

    @Resource
    private CustomerDao customerDao;

    @Autowired
    private LogService logService;

    @Override
    public Map<String, Object> customerList(Integer page, Integer rows, String customerName) {
        Map<String, Object> map = new HashMap<>();

        int total = customerDao.getCustomerCount(customerName);
        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;
        List<Customer> roles = customerDao.customerList(offSet, rows, customerName);

        logService.save(new Log(Log.SELECT_ACTION, "分页查询客户"));

        map.put("total", total);
        map.put("rows", roles);

        return map;
    }

    @Override
    public ServiceVO saveCustomer(Customer customer) {
        //先查看customerId 有的话修改，没有的话新增
        if (customer.getCustomerId() == null) {
            //新增
            Integer insertCustomer = customerDao.insert(customer);
            log.info("新增数据{}", insertCustomer);
            if (insertCustomer == 1) {
                return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
            } else {
                return new ServiceVO(ErrorCode.REQ_ERROR_CODE,ErrorCode.REQ_ERROR_MESS);
            }
        } else {
            //修改
            Integer updateCustomer = customerDao.update(customer);
            log.info("修改数据{}", updateCustomer);
            if (updateCustomer == 1) {
                return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
            } else {
                return new ServiceVO(ErrorCode.REQ_ERROR_CODE,ErrorCode.REQ_ERROR_MESS);
            }
        }
    }

    @Override
    public ServiceVO customerDelete(String ids) {
        //前端返回的是用,分割id的字符串，转换成Integer数组
        int[] ints = Arrays.stream(ids.split(",")).mapToInt(Integer::parseInt).toArray();
        Integer deleteCustomer = customerDao.delete(ints);
        log.info("删除(支持批量)数据{}", deleteCustomer);
        if (deleteCustomer >= 1) {
            return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
        } else {
            return new ServiceVO(ErrorCode.REQ_ERROR_CODE,ErrorCode.REQ_ERROR_MESS);
        }
    }
}
