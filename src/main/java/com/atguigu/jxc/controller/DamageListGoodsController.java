package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.service.DamageListGoodsService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

/**
 * 商品报损
 */
@RestController
@RequestMapping("/damageListGoods")
public class DamageListGoodsController {

    @Autowired
    private DamageListGoodsService damageListGoodsService;

    @PostMapping("/save")
    public ServiceVO saveDamageListGoods(DamageList damageList, String damageListGoodsStr, HttpSession session) {
        //两项数据，分别封装到两张表上，第二张表要把String转换成Json，用gson工具转换
        Gson gson = new Gson();
        Type type = new TypeToken<List<DamageListGoods>>() {
        }.getType();
        List<DamageListGoods> damageListGoodsList = gson.fromJson(damageListGoodsStr, type);
        return damageListGoodsService.saveDamageListGoods(damageList, damageListGoodsList, session);
    }

    @PostMapping("/list")
    public Map<String, Object> list(String sTime, String eTime) {
        return damageListGoodsService.list(sTime, eTime);
    }

    @PostMapping("/goodsList")
    public Map<String, Object> goodsList(Integer damageListId) {
        return damageListGoodsService.goodsList(damageListId);
    }

}
