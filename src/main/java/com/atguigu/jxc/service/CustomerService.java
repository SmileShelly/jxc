package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Customer;

import java.util.Map;

public interface CustomerService {
    Map<String, Object> customerList(Integer page, Integer rows, String customerName);

    ServiceVO saveCustomer(Customer customer);

    ServiceVO customerDelete(String ids);
}
