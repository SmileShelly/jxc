package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.SupplierDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.entity.Role;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.LogService;
import com.atguigu.jxc.service.SupplierService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class SupplierServiceImpl implements SupplierService {

    @Resource
    private SupplierDao supplierDao;

    @Autowired
    private LogService logService;

    @Override
    public Map<String, Object> list(Integer page, Integer rows, String supplierName) {
        Map<String, Object> map = new HashMap<>();

        int total = supplierDao.getSupplierCount(supplierName);
        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;
        List<Supplier> roles = supplierDao.list(offSet, rows, supplierName);

        logService.save(new Log(Log.SELECT_ACTION, "分页查询供应商"));

        map.put("total", total);
        map.put("rows", roles);

        return map;
    }

    @Override
    public ServiceVO save(Supplier supplier) {
        //先查看supplierId 有的话修改，没有的话新增
        if (supplier.getSupplierId() == null) {
            //新增
            Integer insertSupplier = supplierDao.insert(supplier);
            log.info("新增数据{}", insertSupplier);
        } else {
            //修改
            Integer updateSupplier = supplierDao.update(supplier);
            log.info("修改数据{}", updateSupplier);
        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    @Override
    public ServiceVO delete(String ids) {
        //前端返回的是用,分割id的字符串，转换成Integer数组
        int[] ints = Arrays.stream(ids.split(",")).mapToInt(Integer::parseInt).toArray();
        Integer deleteSupplier = supplierDao.delete(ints);
        log.info("删除(支持批量)数据{}", deleteSupplier);
        if (deleteSupplier >= 1) {
            return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
        } else {
            return new ServiceVO(ErrorCode.REQ_ERROR_CODE,ErrorCode.REQ_ERROR_MESS);
        }
    }
}
