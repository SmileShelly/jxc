package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.UnitDao;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.entity.Role;
import com.atguigu.jxc.entity.Unit;
import com.atguigu.jxc.service.LogService;
import com.atguigu.jxc.service.UnitService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class UnitServiceImpl implements UnitService {

    @Resource
    private UnitDao unitDao;

    @Autowired
    private LogService logService;

    @Override
    public Map<String, Object> unitList() {
        Map<String,Object> map = new HashMap<>();

        List<Unit> unitList = unitDao.findAll();

        logService.save(new Log(Log.SELECT_ACTION, "查询所有角色信息"));

        map.put("rows", unitList);

        return map;
    }
}
