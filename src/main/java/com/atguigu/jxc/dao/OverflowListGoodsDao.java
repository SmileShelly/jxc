package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OverflowListGoodsDao {
    Integer saveOverflowList(OverflowList overflowList);

    Integer saveOverflowListGoods(@Param("overflowListGoodsList") List<OverflowListGoods> overflowListGoodsList, @Param("overflowListId") Integer overflowListId);

    List<OverflowList> list(@Param("sTime") String sTime, @Param("eTime") String eTime);

    List<OverflowListGoods> goodsList(@Param("overflowListId") Integer overflowListId);
}
