package com.atguigu.jxc;


import com.atguigu.jxc.dao.*;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.Unit;
import com.atguigu.jxc.service.GoodsService;
import com.atguigu.jxc.service.UnitService;
import com.google.gson.Gson;
import com.sun.org.apache.bcel.internal.generic.NEW;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@SpringBootTest
public class JxcApplicationTests {

	@Test
	public void contextLoads() {
	}

	@Autowired
	private GoodsDao goodsDao;
	@Test
	public void test001(){
		List<Goods> listInventory = goodsDao.getListInventory(0, 1, null, null);
		listInventory.forEach(System.out::println);
	}

	@Test
	public void test002(){
		int goodsCount = goodsDao.getGoodsCount(null, 11);
		System.out.println(goodsCount);
	}

	@Autowired
	private SaleListGoodsDao saleListGoodsDao;

	@Autowired
	private CustomerReturnListGoodsDao customerReturnListGoodsDao;
	@Test
	public void test003(){
		List<Integer> goodsNumByGoodsId = saleListGoodsDao.getGoodsNumByGoodsId(11);
		goodsNumByGoodsId.forEach(System.out::println);

		List<Integer> returnGoodsNumByGoodsId = customerReturnListGoodsDao.getReturnGoodsNumByGoodsId(11);
		returnGoodsNumByGoodsId.forEach(System.out::println);
	}

	@Autowired
	private UnitDao unitDao;

	@Test
	public void test004(){
		List<Unit> all = unitDao.findAll();
		all.forEach(System.out::println);
	}

	@Autowired
	private GoodsService goodsService;

	@Test
	public void test005(){
		List<Goods> noInventoryQuantity = goodsDao.getNoInventoryQuantity(1, 10, null);
		noInventoryQuantity.forEach(System.out::println);
		System.out.println(goodsDao.CountNoInventoryQuantityByInventoryQuantity(null));
	}


}
