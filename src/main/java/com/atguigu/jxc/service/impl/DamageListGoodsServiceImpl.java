package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.DamageListGoodsDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.DamageListGoodsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class DamageListGoodsServiceImpl implements DamageListGoodsService {

    @Resource
    private DamageListGoodsDao damageListGoodsDao;

    @Override
    public ServiceVO saveDamageListGoods(DamageList damageList, List<DamageListGoods> damageListGoodsList, HttpSession session) {
        //对两个对象分别进行新增
        //DamageListGoods对象需要damageListId，要通过MyBatis的主键回填得到
        //todo 前端没有返回userId字段 思路：要通过session来获取userId
        //通过session获取userId
        User user = (User) session.getAttribute("currentUser");
        Integer userId = user.getUserId();
        //将userId存入damageList中
        damageList.setUserId(userId);
        Integer saveDamageList = damageListGoodsDao.saveDamageList(damageList);
        Integer damageListId = damageList.getDamageListId();
        //集合中可能会有多个，所以用foreach动态标签实现
        //将damageListId添加到DamageListGoods对象中
        Integer saveDamageListGoods = damageListGoodsDao.saveDamageListGoods(damageListGoodsList, damageListId);
        if (saveDamageList == 1 && saveDamageListGoods != 0) {
            return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
        }
        return new ServiceVO(ErrorCode.REQ_ERROR_CODE, ErrorCode.REQ_ERROR_MESS);
    }

    @Override
    public Map<String, Object> list(String sTime, String eTime) {
        Map<String, Object> map = new HashMap<>();
        List<DamageList> damageLists = damageListGoodsDao.list(sTime, eTime);
        map.put("rows", damageLists);
        return map;
    }

    @Override
    public Map<String, Object> goodsList(Integer damageListId) {
        Map<String, Object> map = new HashMap<>();
        List<DamageListGoods> damageListGoodsList = damageListGoodsDao.goodsList(damageListId);
        map.put("rows", damageListGoodsList);
        return map;
    }
}
