package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Supplier;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SupplierDao {

    List<Supplier> list(@Param("offSet") Integer offSet, @Param("rows") Integer rows, @Param("supplierName") String supplierName);

    int getSupplierCount(@Param("supplierName") String supplierName);

    Integer insert(Supplier supplier);

    Integer update(Supplier supplier);

    Integer delete(int[] ints);
}
