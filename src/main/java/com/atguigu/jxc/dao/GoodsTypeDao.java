package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @description 商品类别
 */
public interface GoodsTypeDao {

    List<GoodsType> getAllGoodsTypeByParentId(Integer pId);

    Integer updateGoodsTypeState(GoodsType parentGoodsType);

    Integer saveGoodsType(String goodsTypeName, Integer pId);

    Integer selectPIdById(@Param("goodsTypeId") Integer goodsTypeId);

    Integer deleteGoodsType(@Param("goodsTypeId") Integer goodsTypeId);

    Integer countByPId(@Param("pId") Integer pId);
}
