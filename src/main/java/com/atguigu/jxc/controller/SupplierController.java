package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @description 供应商管理Controller
 */
@RestController
@RequestMapping("/supplier")
public class SupplierController {

    @Autowired
    private SupplierService supplierService;

    /**
     * 分页查询供应商
     * @param page
     * @param rows
     * @param supplierName
     * @return
     */
    @PostMapping("/list")
    public Map<String, Object> list(Integer page, Integer rows, String supplierName) {
        return supplierService.list(page, rows, supplierName);
    }

    /**
     * 供应商新增和修改
     * @param supplier
     * @return
     */
    @PostMapping("/save")
    public ServiceVO save(Supplier supplier){
        return supplierService.save(supplier);
    }

    /**
     * 删除供应商（支持批量删除）
     * @param ids
     * @return
     */
    @PostMapping("/delete")
    public ServiceVO delete(String ids){
        return supplierService.delete(ids);
    }
}
