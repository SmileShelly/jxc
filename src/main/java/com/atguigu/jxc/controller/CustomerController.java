package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 *客户管理
 */
@RestController
@RequestMapping("/customer")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    /**
     * 客户列表分页（名称模糊查询）
     * @param page
     * @param rows
     * @param customerName
     * @return
     */
    @PostMapping("/list")
    private Map<String, Object> customerList(Integer page, Integer rows, String customerName){
        return customerService.customerList(page, rows, customerName);
    }

    /**
     * 客户添加或修改
     * @param customer
     * @return
     */
    @PostMapping("/save")
    private ServiceVO saveCustomer(Customer customer){
        return customerService.saveCustomer(customer);
    }

    /**
     * 客户删除（支持批量删除）
     * @param ids
     * @return
     */
    @PostMapping("/delete")
    private ServiceVO customerDelete(String ids){
        return customerService.customerDelete(ids);
    }
}
