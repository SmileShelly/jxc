package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

public interface DamageListGoodsService {

    ServiceVO saveDamageListGoods(DamageList damageList, List<DamageListGoods> damageListGoodsList, HttpSession session);

    Map<String, Object> list(String sTime, String eTime);

    Map<String, Object> goodsList(Integer damageListId);
}
