package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;
import com.atguigu.jxc.service.OverflowListGoodsService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

/**
 * 商品报溢
 */
@RestController
@RequestMapping("/overflowListGoods")
public class OverflowListGoodsController {

    @Autowired
    private OverflowListGoodsService overflowListGoodsService;

    @PostMapping("/save")
    public ServiceVO saveOverflowListGoods(OverflowList overflowList, String overflowListGoodsStr, HttpSession session) {
        //两项数据，分别封装到两张表上，第二张表要把String转换成Json，用gson工具转换
        Gson gson = new Gson();
        Type type = new TypeToken<List<OverflowListGoods>>() {}.getType();
        List<OverflowListGoods> overflowListGoodsList = gson.fromJson(overflowListGoodsStr, type);
        return overflowListGoodsService.saveOverflowListGoods(overflowList, overflowListGoodsList, session);
    }

    @PostMapping("/list")
    public Map<String, Object> list(String sTime, String eTime) {
        return overflowListGoodsService.list(sTime, eTime);
    }

    @PostMapping("/goodsList")
    public Map<String, Object> goodsList(Integer overflowListId) {
        return overflowListGoodsService.goodsList(overflowListId);
    }
}
