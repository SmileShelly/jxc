package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.OverflowListGoodsDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.*;
import com.atguigu.jxc.service.OverflowListGoodsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class OverflowListGoodsServiceImpl implements OverflowListGoodsService {

    @Resource
    private OverflowListGoodsDao overflowListGoodsDao;

    @Override
    public ServiceVO saveOverflowListGoods(OverflowList overflowList, List<OverflowListGoods> overflowListGoodsList, HttpSession session) {
        //对两个对象分别进行新增
        //OverflowListGoods对象需要overflowListId，要通过MyBatis的主键回填得到
        //todo 前端没有返回userId字段 思路：要通过session来获取userId
        //通过session获取userId
        User user = (User) session.getAttribute("currentUser");
        Integer userId = user.getUserId();
        //将userId存入overflowList中
        overflowList.setUserId(userId);
        Integer saveOverflowList = overflowListGoodsDao.saveOverflowList(overflowList);
        Integer overflowListId = overflowList.getOverflowListId();
        //集合中可能会有多个，所以用foreach动态标签实现
        //将overflowListId添加到OverflowListGoods对象中
        Integer saveOverflowListGoods = overflowListGoodsDao.saveOverflowListGoods(overflowListGoodsList, overflowListId);
        if (saveOverflowList == 1 && saveOverflowListGoods != 0) {
            return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
        }
        return new ServiceVO(ErrorCode.REQ_ERROR_CODE, ErrorCode.REQ_ERROR_MESS);
    }

    @Override
    public Map<String, Object> list(String sTime, String eTime) {
        Map<String, Object> map = new HashMap<>();
        List<OverflowList> overflowLists = overflowListGoodsDao.list(sTime, eTime);
        map.put("rows", overflowLists);
        return map;
    }

    @Override
    public Map<String, Object> goodsList(Integer overflowListId) {
        Map<String, Object> map = new HashMap<>();
        List<OverflowListGoods> overflowListGoodsList = overflowListGoodsDao.goodsList(overflowListId);
        map.put("rows", overflowListGoodsList);
        return map;
    }
}
