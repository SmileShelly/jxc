package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @description 商品信息
 */
public interface GoodsDao {


    String getMaxCode();

    List<Goods> getListInventory(@Param("offSet") Integer offSet, @Param("rows") Integer rows, @Param("codeOrName") String codeOrName, @Param("goodsTypeId") Integer goodsTypeId);

    int getGoodsCount(@Param("codeOrName") String codeOrName, @Param("goodsTypeId") Integer goodsTypeId);

    int getGoodsListCount(@Param("goodsName") String goodsName, @Param("goodsTypeId") Integer goodsTypeId);

    List<Goods> getGoodsList(@Param("offSet") Integer offSet, @Param("rows") Integer rows, @Param("goodsName") String goodsName, @Param("goodsTypeId") Integer goodsTypeId);

    Integer insert(Goods goods);

    Integer update(Goods goods);

    Integer getStateByGoodsId(@Param("goodsId") Integer goodsId);

    Integer deleteGoods(@Param("goodsId") Integer goodsId);

    int CountNoInventoryQuantityByInventoryQuantity(@Param("nameOrCode") String nameOrCode);

    List<Goods> getNoInventoryQuantity(@Param("offSet") Integer offSet, @Param("rows") Integer rows, @Param("nameOrCode") String nameOrCode);

    int CountHasInventoryQuantityByInventoryQuantity(@Param("nameOrCode") String nameOrCode);

    List<Goods> getHasInventoryQuantity(@Param("offSet") Integer offSet, @Param("rows") Integer rows, @Param("nameOrCode") String nameOrCode);

    Integer saveStock(@Param("goodsId") Integer goodsId, @Param("inventoryQuantity") Integer inventoryQuantity, @Param("purchasingPrice") double purchasingPrice, @Param("lastPurchasingPrice") double lastPurchasingPrice);

    Double getPurchasingPriceById(@Param("goodsId") Integer goodsId);

    Integer updateStock(@Param("goodsId") Integer goodsId);

    List<Goods> listAlarm();
}
