package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.entity.Supplier;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CustomerDao {
    List<Customer> customerList(@Param("offSet") Integer offSet, @Param("rows") Integer rows, @Param("customerName") String customerName);

    int getCustomerCount(@Param("customerName") String customerName);

    Integer insert(Customer customer);

    Integer update(Customer customer);

    Integer delete(int[] ints);
}
