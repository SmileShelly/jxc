package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsTypeDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.service.GoodsTypeService;
import com.atguigu.jxc.service.LogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @description
 */
@Service
@Slf4j
public class GoodsTypeServiceImpl implements GoodsTypeService {

    @Autowired
    private LogService logService;
    @Autowired
    private GoodsTypeDao goodsTypeDao;

    @Override
    public ArrayList<Object> loadGoodsType() {
        logService.save(new Log(Log.SELECT_ACTION, "查询商品类别信息"));


        return this.getAllGoodsType(-1); // 根节点默认从-1开始
    }

    @Override
    @Transactional
    public ServiceVO saveGoodsType(String goodsTypeName, Integer pId) {
        //如果当前分类的pId的goods_type_state字段为0(叶子结点)，需要把值改为1
        //因为叶子结点下不能再填类别，要变为非叶子结点
        GoodsType goodsType = new GoodsType();
        goodsType.setGoodsTypeId(pId);
        goodsType.setGoodsTypeState(1);
        goodsTypeDao.updateGoodsTypeState(goodsType);
        Integer save = goodsTypeDao.saveGoodsType(goodsTypeName, pId);
        log.info("新增数据{}", save);
        if (save == 1) {
            return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
        } else {
            return new ServiceVO(ErrorCode.REQ_ERROR_CODE, ErrorCode.REQ_ERROR_MESS);
        }
    }

    @Override
    @Transactional
    public ServiceVO deleteGoodsType(Integer goodsTypeId) {
        //先根据Id查对应pId，如果删完之后pId类别下没有其他节点，需要改变goods_type_state字段为0(叶子结点)，有的话不动
        Integer pId = goodsTypeDao.selectPIdById(goodsTypeId);
        //删除当前Id对应类别
        Integer delete = goodsTypeDao.deleteGoodsType(goodsTypeId);
        //再用聚合函数查询以当前pId类别作为父类别的数量，大于等于1说明为非叶子结点，为0说明是叶子结点，需要改变goods_type_state字段
        Integer count = goodsTypeDao.countByPId(pId);
        if (count == 0) {
            GoodsType goodsType = new GoodsType();
            goodsType.setGoodsTypeId(pId);
            goodsType.setGoodsTypeState(0);
            goodsTypeDao.updateGoodsTypeState(goodsType);
        }
        log.info("删除数据{}", delete);
        if (delete == 1) {
            return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
        } else {
            return new ServiceVO(ErrorCode.REQ_ERROR_CODE, ErrorCode.REQ_ERROR_MESS);
        }
    }

    /**
     * 递归查询所有商品类别
     *
     * @return
     */
    public ArrayList<Object> getAllGoodsType(Integer parentId) {

        ArrayList<Object> array = this.getGoodSTypeByParentId(parentId);

        for (int i = 0; i < array.size(); i++) {

            HashMap obj = (HashMap) array.get(i);

            if (obj.get("state").equals("open")) {// 如果是叶子节点，不再递归

            } else {// 如果是根节点，继续递归查询
                obj.put("children", this.getAllGoodsType(Integer.parseInt(obj.get("id").toString())));
            }

        }

        return array;
    }

    /**
     * 根据父ID获取所有子商品类别
     *
     * @return
     */
    public ArrayList<Object> getGoodSTypeByParentId(Integer parentId) {

        ArrayList<Object> array = new ArrayList<>();

        List<GoodsType> goodsTypeList = goodsTypeDao.getAllGoodsTypeByParentId(parentId);

        System.out.println("goodsTypeList" + goodsTypeList);
        //遍历商品类别
        for (GoodsType goodsType : goodsTypeList) {

            HashMap obj = new HashMap<String, Object>();

            obj.put("id", goodsType.getGoodsTypeId());
            obj.put("text", goodsType.getGoodsTypeName());

            if (goodsType.getGoodsTypeState() == 1) {
                obj.put("state", "closed");

            } else {
                obj.put("state", "open");
            }

            obj.put("iconCls", "goods-type");

            HashMap<String, Object> attributes = new HashMap<>();
            attributes.put("state", goodsType.getGoodsTypeState());
            obj.put("attributes", attributes);

            array.add(obj);

        }

        return array;
    }
}
