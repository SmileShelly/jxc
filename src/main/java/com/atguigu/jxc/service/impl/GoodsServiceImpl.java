package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.CustomerReturnListGoodsDao;
import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.dao.SaleListGoodsDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.entity.SaleListGoods;
import com.atguigu.jxc.service.CustomerReturnListGoodsService;
import com.atguigu.jxc.service.GoodsService;
import com.atguigu.jxc.service.LogService;
import com.atguigu.jxc.service.SaleListGoodsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @description
 */
@Service
@Slf4j
@Transactional
public class GoodsServiceImpl implements GoodsService {

    @Resource
    private GoodsDao goodsDao;

    @Autowired
    private LogService logService;

    @Resource
    private SaleListGoodsDao saleListGoodsDao;

    @Resource
    private CustomerReturnListGoodsDao customerReturnListGoodsDao;

    @Override
    public ServiceVO getCode() {

        // 获取当前商品最大编码
        String code = goodsDao.getMaxCode();

        // 在现有编码上加1
        Integer intCode = Integer.parseInt(code) + 1;

        // 将编码重新格式化为4位数字符串形式
        String unitCode = intCode.toString();

        for (int i = 4; i > intCode.toString().length(); i--) {

            unitCode = "0" + unitCode;

        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS, unitCode);
    }

    @Override
    public Map<String, Object> getListInventory(Integer page, Integer rows, String codeOrName, Integer goodsTypeId) {
        Map<String, Object> map = new HashMap<>();
        //count函数判断总数，封装到map中
        int total = goodsDao.getGoodsCount(codeOrName, goodsTypeId);
        //将Integer page（当前页数）, Integer rows（每页显示的记录数）转换成SQL中limit需要的参数
        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;
        //goods与goodsType联查，获取除销售总数以外的数据
        List<Goods> listInventory = goodsDao.getListInventory(offSet, rows, codeOrName, goodsTypeId);
        //将集合遍历，求出每个goodsId对应的销售数量 - 退货数量 得到销售总量封装到对应的list集合中
        for (Goods goods : listInventory) {
            Integer goodsId = goods.getGoodsId();
            //销售数量与退货数量需要根据goodsId遍历，将同一Id的全部数量获取到
            Integer goodsNum = saleListGoodsDao.getGoodsNumByGoodsId(goodsId).stream().reduce(0, Integer::sum);
            Integer returnGoodsNum = customerReturnListGoodsDao.getReturnGoodsNumByGoodsId(goodsId).stream().reduce(0, Integer::sum);
            goods.setSaleTotal(goodsNum - returnGoodsNum);
        }

        logService.save(new Log(Log.SELECT_ACTION, "分页查询商品库存信息"));

        log.info("首页返回结果{}", listInventory);
        //封装结果
        map.put("total", total);
        map.put("rows", listInventory);

        return map;
    }

    @Override
    public Map<String, Object> getGoodsList(Integer page, Integer rows, String goodsName, Integer goodsTypeId) {
        Map<String, Object> map = new HashMap<>();
        //count函数判断总数，封装到map中
        int total = goodsDao.getGoodsListCount(goodsName, goodsTypeId);
        //将Integer page（当前页数）, Integer rows（每页显示的记录数）转换成SQL中limit需要的参数
        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;
        //goods与goodsType联查，获取除销售总数以外的数据
        List<Goods> goodsList = goodsDao.getGoodsList(offSet, rows, goodsName, goodsTypeId);

        logService.save(new Log(Log.SELECT_ACTION, "查询所有商品信息"));

        log.info("查询所有商品信息结果{}", goodsList);
        //封装结果
        map.put("total", total);
        map.put("rows", goodsList);

        return map;
    }

    @Override
    public ServiceVO saveGoods(Goods goods) {
        //先看有无goodsId，没有是新增，有是修改
        if (goods.getGoodsId() == null) {
            //新增
            Integer insertGoods = goodsDao.insert(goods);
            log.info("新增数据{}", insertGoods);
            if (insertGoods == 1) {
                return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
            }
        } else {
            //修改
            Integer updateGoods = goodsDao.update(goods);
            log.info("修改数据{}", updateGoods);
            if (updateGoods == 1) {
                return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
            }
        }
        return new ServiceVO(ErrorCode.REQ_ERROR_CODE, ErrorCode.REQ_ERROR_MESS);
    }

    @Override
    public ServiceVO deleteGoods(Integer goodsId) {
        //先看state字段
        Integer state = goodsDao.getStateByGoodsId(goodsId);
        if (state == 0) {
            //state字段为0可以删
            Integer delete = goodsDao.deleteGoods(goodsId);
            if (delete == 1) {
                return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
            }
        } else if (state == 1) {
            //为1表示已入库，不能删
            return new ServiceVO<>(ErrorCode.STORED_ERROR_CODE, ErrorCode.STORED_ERROR_MESS);
        } else if (state == 2) {
            //为2表示有进货和销售单据，不能删
            return new ServiceVO<>(ErrorCode.HAS_FORM_ERROR_CODE, ErrorCode.HAS_FORM_ERROR_MESS);
        }
        return new ServiceVO(ErrorCode.REQ_ERROR_CODE, ErrorCode.REQ_ERROR_MESS);
    }

    @Override
    public Map<String, Object> getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        Map<String, Object> map = new HashMap<>();
        //count函数判断总数，封装到map中
        //无库存，库存<=0
        int total = goodsDao.CountNoInventoryQuantityByInventoryQuantity(nameOrCode);
        //将Integer page（当前页数）, Integer rows（每页显示的记录数）转换成SQL中limit需要的参数
        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;
        //无库存，库存<=0
        List<Goods> goodsList = goodsDao.getNoInventoryQuantity(offSet, rows, nameOrCode);

        logService.save(new Log(Log.SELECT_ACTION, "查询无库存商品列表"));

        log.info("查询无库存商品列表结果{}", goodsList);
        //封装结果
        map.put("total", total);
        map.put("rows", goodsList);

        return map;
    }

    @Override
    public Map<String, Object> getHasInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        Map<String, Object> map = new HashMap<>();
        //count函数判断总数，封装到map中
        //有库存，库存>0
        int total = goodsDao.CountHasInventoryQuantityByInventoryQuantity(nameOrCode);
        //将Integer page（当前页数）, Integer rows（每页显示的记录数）转换成SQL中limit需要的参数
        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;
        //有库存，库存>0
        List<Goods> goodsList = goodsDao.getHasInventoryQuantity(offSet, rows, nameOrCode);

        logService.save(new Log(Log.SELECT_ACTION, "查询有库存商品列表"));

        log.info("查询有库存商品列表结果{}", goodsList);
        //封装结果
        map.put("total", total);
        map.put("rows", goodsList);

        return map;
    }

    @Override
    public ServiceVO saveStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice) {
        //要把当前价格录入到上次价格字段
        //查询当次价格
        Double lastPurchasingPrice = goodsDao.getPurchasingPriceById(goodsId);
        //再把当前价格赋到上次价格上
        Integer saveStock = goodsDao.saveStock(goodsId, inventoryQuantity, purchasingPrice, lastPurchasingPrice);
        if (saveStock == 1) {
            return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
        }
        return new ServiceVO(ErrorCode.REQ_ERROR_CODE,ErrorCode.REQ_ERROR_MESS);
    }

    @Override
    public ServiceVO deleteStock(Integer goodsId) {
        //先看state字段
        Integer state = goodsDao.getStateByGoodsId(goodsId);
        if (state == 0) {
            //state字段为0可以将库存字段改成0
            Integer update = goodsDao.updateStock(goodsId);
            if (update == 1) {
                return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
            }
        } else if (state == 1) {
            //为1表示已入库，不能删
            return new ServiceVO<>(ErrorCode.STORED_ERROR_CODE, ErrorCode.STORED_ERROR_MESS);
        } else if (state == 2) {
            //为2表示有进货和销售单据，不能删
            return new ServiceVO<>(ErrorCode.HAS_FORM_ERROR_CODE, ErrorCode.HAS_FORM_ERROR_MESS);
        }
        return new ServiceVO(ErrorCode.REQ_ERROR_CODE, ErrorCode.REQ_ERROR_MESS);
    }

    @Override
    public Map<String, Object> listAlarm() {
        Map<String, Object> map = new HashMap<>();
        List<Goods> rows = goodsDao.listAlarm();
        map.put("rows", rows);
        return map;
    }

}
