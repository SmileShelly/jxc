package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

public interface OverflowListGoodsService {
    ServiceVO saveOverflowListGoods(OverflowList overflowList, List<OverflowListGoods> overflowListGoodsList, HttpSession session);

    Map<String, Object> list(String sTime, String eTime);

    Map<String, Object> goodsList(Integer overflowListId);
}
